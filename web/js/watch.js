// debug
tp = { debug: false };

// we setup configuration
tp.config =
{
    realm: 'confeatu.re',
    impi: 'johndoe',
    display_name: 'John Doe',
    job_title: 'Technology Evangelist',
    websocket_proxy_url: 'ws://88.191.188.178:20060',
    http_server_url: 'http://88.191.188.178:20065',
    organization: 'Confeatu.re',
    audio_velocity: '[0.0f, 0.0f, 0.0f]',
    audio_position: '[0.0f, 0.0f, 0.0f]',
    sip_outbound_proxy_url: null,
    bandwidth: { audio:undefined, video:undefined },
    video_size: { minWidth:undefined, minHeight:undefined, maxWidth:undefined, maxHeight:undefined },
    ice_servers: null,
    mute: false
};

tp.started = false;
tp.callSession = null;

tp.init = function () {
    tsk_utils_log_info('[Confeature] tp.init()');

    tp.audio_remote = document.createElement('audio');
    tp.audio_remote.autoplay = "autoplay";

    tp.div_glass = document.getElementById('tp_div_glass');

    document.body.appendChild(tp.audio_remote);

    SIPml.init(
	    function (e) { // successCallback
		document.body.style.cursor = 'default';
	    },
	    function (e) { // errorCallback
		tp_lbl_status.innerHTML = e.description;
		document.body.style.cursor = 'default';
	    }
	);
}

tp.fullscreen = function (start) {
    if (tp_video_remote.webkitSupportsFullscreen) {
	if (start) {
	    tp_video_remote.webkitEnterFullScreen();
	}
	else {
	    tp_video_remote.webkitExitFullscreen();
	}
    }
}

tp.call = function () {
    tsk_utils_log_info('[Confeature] tp.call()');

    if (tp.callSession) {
	tp.callSession.hangup();
	return;
    }

    var from = null; //'\"'+tp.config.display_name+'\"<sip:'+tp.config.impi+'@'+tp.config.realm+'>';
    var to = "1234";

    tp.stack.setConfiguration({
	display_name: tp.config.display_name
	// more new values here
    });

    tsk_utils_log_info('[Confeature] making call from [' + from + '] to [' + to + ']...');

    var call_listener = function (e) {
	tsk_utils_log_info('[Confeature] session event = ' + e.type);
	switch (e.type) {
	    case 'connecting': case 'connected':
		{
		    if (e.session === tp.callSession) {
			if(e.type === 'connected') {
			    tp_lbl_status.innerHTML = 'in call';
			}
			else { // connecting
			    tp_lbl_status.innerHTML = 'joining...';
			    tp.config.mute = false; tp_btn_mute.value = "mute";
			}
		    }
		    break;
		}
	    case 'i_ao_request':
		{
		    if (e.session === tp.callSession) {
			var code = e.getSipResponseCode();
			if (code == 180 || code == 183) {
			    tp_lbl_status.innerHTML = 'ringing...';
			}
		    }
		    break;
		}
	    case 'm_early_media':
		{
		    if (e.session === tp.callSession) {
			tp_lbl_status.innerHTML = 'early media...';
		    }
		    break;
		}
	    case 'm_stream_video_local_added':
	    case 'm_stream_video_local_removed':
	    case 'm_stream_video_remote_added':
	    case 'm_stream_video_remote_removed':
		{
		    if (e.session === tp.callSession) {
			if (e.type === 'm_stream_video_local_added') tp_video_local.style.opacity = 1;
			else if (e.type === 'm_stream_video_local_removed') tp_video_local.style.opacity = 0;
			else if (e.type === 'm_stream_video_remote_added') { 
			    tp_video_remote.style.opacity = 1; 
			    tp_div_call_options.style.visibility = 'visible'; 
			    tp_div_call.style.visibility = 'hidden';
			}
			else if (e.type === 'm_stream_video_remote_removed') { 
			    tp_video_remote.style.opacity = 0; 
			    tp.fullscreen(false);
			}
		    }
		    break;
		}
	    case 'terminating': case 'terminated':
		{
		    if (e.session === tp.callSession) {
			tp_lbl_status.innerHTML = e.description.toLowerCase();
			tp.callSession = null;
			tp.div_glass.style.visibility = 'hidden';
			tp_div_call_options.style.visibility = 'hidden';
			tp_div_call.style.visibility = 'visible';
			tp_video_local.style.opacity = tp_video_remote.style.opacity = 0;
			tp.fullscreen(false);
			window.setTimeout(function () { tp_lbl_status.innerHTML = ''; }, 2000);
		    }
		    break;
		}
	}
    };

    tp.callSession = tp.stack.newSession('suscribe', {
	from: from,
	audio_remote: tp.audio_remote,
	video_local: /* tp_video_local */null,
	video_remote: tp_video_remote,
	bandwidth: tp.config.bandwidth,
	video_size: tp.config.video_size,
	events_listener: { events: '*', listener: call_listener },
	sip_caps: [
				{ name: '+g.oma.sip-im' },
				{ name: '+sip.ice' },
				{ name: 'language', value: '\"en,fr\"' }
			    ],
	sip_headers: [
		{ name: 'TP-JobTitle', value: tp.config.job_title, session: true },
		{ name: 'TP-BridgePin', value: "1234", session: true },
		{ name: 'TP-AudioPosition', value: tp.config.audio_position, session: true },
		{ name: 'TP-AudioVelocity', value: tp.config.audio_velocity, session: true },
		{ name: 'Organization', value: tp.config.organization, session: true }
	]
    });
    tp.callSession.call(to);
}

tp.hangup = function() {
    if(tp.callSession) {
	tp.callSession.hangup();
    }
}

tp.parseInt = function(s_str, i_default) {
    try{ return parseInt(s_str); }
    catch(e){ return i_default; }
}

tp.place_div = function (div, s_location, b_make_visible) {
    if(b_make_visible) {
	div.style.visibility = 'visible';
    }
    switch(s_location) {
	case 'top-center':
	default:
	    {
		div.style.top = 5 + 'px';
		div.style.left = ((document.body.clientWidth - div.clientWidth) >> 1) + 'px';
		break;
	    }
	case 'top-right':
	    {
		div.style.top = 5 + 'px';
		div.style.left = (document.body.clientWidth - div.clientWidth - 20) + 'px';
		break;
	    }
	case 'top-left':
	    {
		div.style.top = 5 + 'px';
		div.style.left = '20px';
		break;
	    }
	case 'center-center':
	    {
		div.style.top = ((document.body.clientHeight - div.clientHeight) >> 1) + 'px';
		div.style.left = ((document.body.clientWidth - div.clientWidth) >> 1) + 'px';
		break;
	    }
	    
    }
}

tp.close_div = function (div) {
    div.style.visibility = 'hidden';
}

tp.mute_toggle = function() {
    if(tp.callSession) {
	if(tp.callSession.info(JSON.stringify({action: 'req_call_mute', enabled: !tp.config.mute}), 'application/json') == 0) {
	    tp.config.mute =!tp.config.mute; // FIXME: wait for 'res_call_mute'
	    tp_btn_mute.value = tp.config.mute ? "unmute" : "mute";
	}
    }
}

document.addEventListener('DOMContentLoaded', function () {
    tp.init();
});

window.onload = function () {
	
	tp.place_div(tp_div_call_options, 'top-left');

	if (!tp.stack) {

	    tsk_utils_log_info(
		'[Confeature] realm=[' + tp.config.realm + '], impi=[' + tp.config.impi + '],' 
		+ 'ws_url=[' + tp.config.websocket_proxy_url + '],'
		+ 'ice_servers=' +JSON.stringify(tp.config.ice_servers) + ','
		+ 'video_size=' + JSON.stringify(tp.config.video_size) + ','
		+ 'bandwidth=' + JSON.stringify(tp.config.bandwidth)
	    );

	    tp.stack = new SIPml.Stack({ realm: tp.config.realm, impi: tp.config.impi, impu: 'sip:' + tp.config.impi + '@' + tp.config.realm, password: 'mysecret',
		events_listener: { events: '*', listener: function (e) {
		    tsk_utils_log_info('[Confeature] stack event = ' + e.type);

		    switch (e.type) {
			case 'starting':
			    {
				tp_lbl_status.innerHTML = 'connecting to confeature...';
				break;
			    }
			case 'started':
			    {
				tp_lbl_status.innerHTML = 'connected';
				tp.started = true;
				tp.call();
				break;
			    }
			case 'stopped':
			case 'stopping':
			    {
				tp.callSession = null;
				tp.started = false;
				tp_lbl_status.innerHTML = (e.type === 'stopped') ? 'disconnected' : 'disconnecting...';
				tp_div_call_options.style.visibility = 'hidden';
				tp_video_local.style.opacity = tp_video_remote.style.opacity = 0;
				tp.fullscreen(false);
				if (e.type === 'stopped') {
				    tp.callSession = null;
				    tp.stack = null;
				}
				break;
			    }
			case 'failed_to_start':
			    {
				tp_lbl_status.innerHTML = 'failed to connect';
				break;
			    }
			    break;
		    } //switch
		} //callback
		}, //events_listener
		display_name: tp.config.display_name,
		enable_rtcweb_breaker: false,
		enable_click2call: false,
		websocket_proxy_url: tp.config.websocket_proxy_url,
		outbound_proxy_url: tp.config.sip_outbound_proxy_url,
		ice_servers: tp.config.ice_servers,
		bandwidth: tp.config.bandwidth,
		video_size: tp.config.video_size
	    }/*stack-config*/);
	}
	if (!tp.started) {
	    tp.stack.start();
	}
	else {
	    tp.call();
	}
}