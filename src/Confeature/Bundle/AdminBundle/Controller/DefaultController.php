<?php

namespace Confeature\Bundle\AdminBundle\Controller;

use Confeature\Bundle\AdminBundle\Form\Type\UserType;
use Confeature\Bundle\AdminBundle\Form\Type\ConferenceType;
use Confeature\Bundle\AdminBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Confeature\Bundle\ConferenceBundle\Entity\Conference;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Util\SecureRandom;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\SecurityContext;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $confs = $this->getDoctrine()
            ->getRepository('ConfeatureConferenceBundle:Conference')
            ->findAll();
        $users = $this->getDoctrine()
            ->getRepository('ConfeatureAdminBundle:User')
            ->findAll();

        return $this->render('ConfeatureAdminBundle:Admin:index.html.twig',
            array('conferences' => $confs,'users' => $users));

    }

    public function createAction()
    {
        return $this->render('ConfeatureAdminBundle:Admin:create.html.twig');
    }

    private function fillConfFromArray($conf, $array)
    {
        $conf->setTitle($array['title']);

        $conf->setDescription("No Description for this conference");
        if(($array['description'] != "")&&(isset($array['description'])))
        {
            $conf->setDescription($array['description']);
        }
        $conf->setMaxViewers($array['max_viewers']);
        $conf->setMaxSpeakers($array['max_speakers']);
        $conf->setState(0);
        if ($array['privacy'] == "protected") {
            $conf->setPrivacy(1);
        } else {
            $conf->setPrivacy(0);
        }

        $conf->setMaxResolution($array['max_resolution']);
        if (isset($array['prioritymethod'])) {
            if ($array['prioritymethod'] == 'Automatic') {
                $conf->setPriorityMethod(0);
            } else {
                $conf->setPriorityMethod(1);
            }
        }
        if (isset($array['noactivity'])) {
            if ($array['noactivity'] == "on") {
                $conf->setInactivityToggle($array['noactivityduration']);
            }
        }
        else {
            $conf->setInactivityToggle(-1);
        }
        if (isset($array['endinghour'])) {
            if ($array['endinghour'] == "on") {
                $conf->setEndingHour(new \DateTime ($array['endinghourvalue']));
            } else {
                $conf->setEndingHour(new \DateTime ($array['endinghourvalue']));
            }
        }
        if (isset($array['nospeakersleft'])) {
            if ($array['nospeakersleft'] == "on") {
                $conf->setStopWhenNoSpeakers(1);
            } else {
                $conf->setStopWhenNoSpeakers(0);
            }
        }
        if (isset($array['timer'])) {
            if ($array['timer'] == "on") {
                $conf->setTimerToggle($array['timerduration']);
            }else{
                $conf->setTimerToggle(-1);
            }
        }else {
            $conf->setTimerToggle(-1);
        }

    }

    public function newAction()
    {
       /* if(sizeof($_POST)<=0){
            return $this->render('ConfeatureConferenceBundle:Error:notfound.html.twig');
        }
        $conf = new Conference();

        $this->fillConfFromArray($conf, $_POST);

        $conf->setStarted(new \DateTime());
        $conf->setConnectedViewers(0);
        $conf->setConnectedSpeakers(0);

        $generator = new SecureRandom();

        $conf->setViewPass(bin2hex($generator->nextBytes(3)));

        $conf->setStreamPass(bin2hex($generator->nextBytes(3)));

        $em = $this->getDoctrine()->getManager();
        $em->persist($conf);
        $em->flush();


        $routeName = $this->container->get('request')->get('_route');
        if ($routeName == "confeature_admin_new") {
            return $this->render('ConfeatureAdminBundle:Admin:new.html.twig', array('conf' => $conf));


        } else if ($routeName == "confeature_admin_new_json") {
            return new JsonResponse(array('id' => $conf->getId(), 'StreamPass' => $conf->getStreamPass()));
        }*/
        $api = $this->get('confeature_api_services');
        $conf = $api->newConference();
        if(conf!=-1)
        {
            return $this->render('ConfeatureAdminBundle:Admin:new.html.twig', array('conf' => $conf));
        }else{
            return $this->render('ConfeatureConferenceBundle:Error:notfound.html.twig');
        }

    }

    public function listAction()
    {
        $confs = $this->getDoctrine()
            ->getRepository('ConfeatureConferenceBundle:Conference')
            ->findAll();

        return $this->render('ConfeatureAdminBundle:Admin:list.html.twig', array('conferences' => $confs));

    }

    public function settingsAction($id)
    {
        $conf = $this->getDoctrine()
            ->getRepository('ConfeatureConferenceBundle:Conference')
            ->find($id);
        return $this->render('ConfeatureAdminBundle:Admin:settings.html.twig', array('conf' => $conf));
    }

    public function editAction($id)
    {
        $conf = $this->getDoctrine()
            ->getRepository('ConfeatureConferenceBundle:Conference')
            ->find($id);
        $em = $this->getDoctrine()->getManager();
        if (isset($_POST['save'])) {
            $this->fillConfFromArray($conf, $_POST);
            return $this->render('ConfeatureAdminBundle:Admin:new.html.twig', array('conf' => $conf));
        }
        if (isset($_POST['delete'])) {
            $em->remove($conf);
        }

        $em->flush();

        return $this->redirect($this->generateUrl('confeature_admin_homepage'));
    }

    public function loginAction()
    {

        $request = $this->getRequest();
        $session = $request->getSession();
        // get the login error if there is one
        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
        } else {
            $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
            $session->remove(SecurityContext::AUTHENTICATION_ERROR);
        }
        return $this->render('ConfeatureAdminBundle:Security:login.html.twig', array(
            // last username entered by the user
            'last_username' => $session->get(SecurityContext::LAST_USERNAME),
            'error' => $error,
        ));
    }


    public function createUserAction()
    {
        $em = $this->getDoctrine()->getEntityManager();

        $factory = $this->get('security.encoder_factory');
        $user = new User();

        $form = $this->createForm(new UserType(), $user);

        $form->handleRequest($this->getRequest());

        if ($form->isValid()) {
            $data = $form->getData();
            $encoder = $factory->getEncoder($user);
            $password = $encoder->encodePassword($data->getPassword(), $user->getSalt());
            $user->setPassword($password);

            $em->persist($data);
            $em->flush();

            //return $this->redirect($this->generateUrl('confeature_admin_login'));
            return $this->redirect($this->generateUrl('confeature_admin_confirm'));

        }

        return $this->render('ConfeatureAdminBundle:Admin:register.html.twig', array('form' => $form->createView()));
    }

    public function confirmAction()
    {

        return $this->render('ConfeatureAdminBundle:Admin:confirm.html.twig');

    }
    public function activateAction($id)
    {
        $user = $this->getDoctrine()
            ->getRepository('ConfeatureAdminBundle:User')
            ->find($id);
        $user->setIsActive(true);
        $em = $this->getDoctrine()->getEntityManager();
        $em->persist($user);
        $em->flush();

        return $this->redirect($this->generateUrl('confeature_admin_homepage'));
    }

    public function deleteAction($id)
    {
        $user = $this->getDoctrine()
            ->getRepository('ConfeatureAdminBundle:User')
            ->find($id);
        $user->setIsActive(true);
        $em = $this->getDoctrine()->getEntityManager();
        $em->remove($user);
        $em->flush();

        return $this->redirect($this->generateUrl('confeature_admin_homepage'));
    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     *
     */
    public function editConferenceAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $routeName = $this->container->get('request')->get('_route');
        if($routeName == "confeature_admin_creation")
        {
            $mode  = "create";
            $conf = new Conference();
        }elseif($routeName == "confeature_admin_settings"){
            $mode = "edit";
            $conf = $this->getDoctrine()
                ->getRepository('ConfeatureConferenceBundle:Conference')
                ->find($id);
        }
        $form = $this->createForm(new ConferenceType(), $conf);

        $form->handleRequest($this->getRequest());

        if ($form->isValid()) {
            $data = $form->getData();

            $em->persist($data);
            $em->flush();

            return $this->render('ConfeatureAdminBundle:Admin:new.html.twig', array('conf' => $conf));
        }

        return $this->render('ConfeatureAdminBundle:Admin:confForm.html.twig', array('form' => $form->createView() , 'mode'=> $mode));
    }

    public function deleteConferenceAction($id)
    {

        $api = $this->get('confeature_api_services');
        $result = $api->deleteConference($id);
        if($result == 1){
            return $this->redirect($this->generateUrl('confeature_admin_homepage'));
        }else{
            return $this->render('ConfeatureConferenceBundle:Error:notfound.html.twig');
        }
    }

    public function toggleConferenceAction($id)
    {
        $api = $this->get('confeature_api_services');
        $api->toggleConference($id);
        return $this->redirect($this->generateUrl('confeature_admin_homepage'));
    }

}
