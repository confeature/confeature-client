<?php namespace Confeature\Bundle\AdminBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ConferenceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title', 'text');
        $builder->add('description', 'text');
        $builder->add('maxSpeakers', 'integer');
        $builder->add('maxViewers', 'integer');

        $builder->add('timerDuration', 'integer');
        $builder->add('inactivityDuration', 'integer');
        $builder->add('endinghour', 'time');

        $builder->add('privacy', 'checkbox', array(
            'label'     => 'Password protected',
            'required'  => false,
        ));

        $builder->add('maxResolution', 'choice', array(
            'choices'   => array(
                '240'   => '240p',
                '480'   => '480p',
                '720'   => '720p',
                '1080'   => '1080p',
            ),
            'required'    => true,
            'expanded'    => true,
            'multiple'   => false
        ));

        $builder->add('noSpeakersToggle', 'checkbox', array(
            'label'     => '',
            'required'  => false,
        ));
        $builder->add('inactivityToggle', 'checkbox', array(
            'label'     => '',
            'required'  => false,
        ));
        $builder->add('timerToggle', 'checkbox', array(
            'label'     => '',
            'required'  => false,
        ));
        $builder->add('endingHourToggle', 'checkbox', array(
            'label'     => '',
            'required'  => false,
        ));

    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Confeature\Bundle\ConferenceBundle\Entity\Conference'
        ));
    }

    public function getName()
    {
        return 'conf';
    }
}