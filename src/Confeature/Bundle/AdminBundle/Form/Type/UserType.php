<?php namespace Confeature\Bundle\AdminBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('Fullname', 'text');
        $builder->add('Username', 'text');
        $builder->add('Email', 'email');
        $builder->add('Password', 'repeated', array(
            'first_name' => 'password',
            'second_name' => 'confirm',
            'type' => 'password',
        ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Confeature\Bundle\AdminBundle\Entity\User'
        ));
    }

    public function getName()
    {
        return 'user';
    }
}