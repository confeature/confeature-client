<?php

namespace Confeature\Bundle\APIBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Confeature\Bundle\ConferenceBundle\Entity\Conference;
use  Symfony\Component\Security\Core\Util\SecureRandom;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('ConfeatureAPIBundle:Default:index.html.twig', array('name' => $name));
    }

    private function fillConfFromArray($conf, $array)
    {
        $conf->setTitle($array['title']);

        $conf->setDescription("No Description for this conference");
        if (isset($array['description'])) {
            if (($array['description'] != "")) {
                $conf->setDescription($array['description']);
            }
        }
        $conf->setMaxViewers($array['maxViewers']);
        $conf->setMaxSpeakers($array['maxSpeakers']);


        if ($array['privacy'] == "protected") {
            $conf->setPrivacy(1);
        } else {
            $conf->setPrivacy(0);
        }

        $conf->setMaxResolution($array['maxResolution']);

        $conf->setInactivityToggle($array['inactivityToggle']);
        $conf->setNoSpeakersToggle($array['noSpeakersToggle']);
        $conf->setTimerToggle($array['timerToggle']);
        $conf->setEndingHourToggle($array['endingHourToggle']);

        $conf->setInactivityDuration($array['inactivityDuration']);
        $conf->setTimerDuration($array['timerDuration']);
        $conf->setEndingHour(new \DateTime($array['endingHour']));

    }

    public function newConference()
    {


        if (sizeof($_POST) <= 0) {
            return null;
        }
        $conf = new Conference();

        $this->fillConfFromArray($conf, $_POST);
        $conf->setState(0);
        $conf->setStarted(new \DateTime());
        $conf->setConnectedViewers(0);
        $conf->setConnectedSpeakers(0);

        $generator = new SecureRandom();

        $conf->setViewPass(bin2hex($generator->nextBytes(3)));

        $conf->setStreamPass(bin2hex($generator->nextBytes(3)));

        $em = $this->getDoctrine()->getManager();
        $em->persist($conf);
        $em->flush();

        return $conf;

    }

    public function conferenceCreateAction()
    {


        if ($this->get('security.context')->isGranted('ROLE_USER')) {
            $conf = $this->newConference();
            if ($conf == null) {
                return new JsonResponse(array('id' => '0', 'StreamPass' => null, 'ViewPass' => null, 'status' => "not enough arguments"));
            } else {
                return new JsonResponse(array('id' => $conf->getId(), 'StreamPass' => $conf->getStreamPass(), 'ViewPass' => $conf->getViewPass(), 'status' => "success"));
            }
        } else {
            return new JsonResponse(array('conf' => null, 'status' => "permission denied"));
        }
    }

    public function conferenceGetAction($id)
    {
        if ($this->get('security.context')->isGranted('ROLE_USER')) {
            $conf = $this->getDoctrine()
                ->getRepository('ConfeatureConferenceBundle:Conference')
                ->find($id);
            if ($conf == null) {
                return new JsonResponse(array('conf' => null, 'status' => "conference not found"));
            } else {
                return new JsonResponse(array('conf' => $conf->toArray(), 'status' => "success"));

            }
        } else {
            return new JsonResponse(array('conf' => null, 'status' => "permission denied"));
        }

    }

    public function conferenceGetAllAction()
    {
        if ($this->get('security.context')->isGranted('ROLE_USER')) {
            $confs = $this->getDoctrine()
                ->getRepository('ConfeatureConferenceBundle:Conference')
                ->findAll();
            $confsarray = array();
            foreach ($confs as $key => $conf) {
                $confsarray[$key] = $conf->toArray();
            }

            return new JsonResponse(array('confs' => $confsarray, 'status' => "success"));

        } else {
            return new JsonResponse(array('conf' => null, 'status' => "Permission Denied, please log in"));
        }

    }


    public function conferenceGetOnlineAction()
    {
        $confs = $this->getDoctrine()
            ->getRepository('ConfeatureConferenceBundle:Conference')
            ->findBy(array('state' => '1'));
        $confsarray = array();
        if ($this->get('security.context')->isGranted('ROLE_USER')) {

            foreach ($confs as $key => $conf) {
                $confsarray[$key] = $conf->toArray();
            }


        } else {
            foreach ($confs as $key => $conf) {
                $confsarray[$key] = $conf->toPublicArray();
            }
        }
        return new JsonResponse(array('confs' => $confsarray, 'status' => "success"));
    }

    public function deleteConference($id)
    {
        $conf = $this->getDoctrine()
            ->getRepository('ConfeatureConferenceBundle:Conference')
            ->find($id);
        if ($conf == null) {
            return -1;
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove($conf);
        $em->flush();

        return 1;

    }

    public function conferenceDeleteAction($id)
    {
        if ($this->get('security.context')->isGranted('ROLE_USER')) {
            $result = $this->deleteConference($id);
            if ($result == 1) {
                return new JsonResponse(array('status' => "success"));
            } else {
                return new JsonResponse(array('status' => "conference not found"));
            }
        } else {
            return new JsonResponse(array('conf' => null, 'status' => "permission denied"));
        }


    }

    public function conferenceEditAction($id)
    {

        if ($this->get('security.context')->isGranted('ROLE_USER')) {
            $conf = $this->getDoctrine()
                ->getRepository('ConfeatureConferenceBundle:Conference')
                ->find($id);
            if ($conf == null) {
                return new JsonResponse(array('id' => '0', 'StreamPass' => null, 'ViewPass' => null, 'status' => "Conference not found"));
            }
            $this->fillConfFromArray($conf, $_POST);

            $em = $this->getDoctrine()->getManager();
            $em->persist($conf);
            $em->flush();

            return new JsonResponse(array('id' => $conf->getId(), 'StreamPass' => $conf->getStreamPass(), 'ViewPass' => $conf->getViewPass(), 'status' => "success"));
        } else {
            return new JsonResponse(array('conf' => null, 'status' => "permission denied"));
        }
    }

    public function userLoginAction()
    {

        $username = $_POST['username'];
        $password = $_POST['password'];
        $repository = $this->getDoctrine()->getRepository('ConfeatureAdminBundle:User');
        $user = $repository->findOneByUsername($username);

        $factory = $this->get('security.encoder_factory');

        $encoder = $factory->getEncoder($user);

        $password = $encoder->encodePassword($password, $user->getSalt());
        if ($password == $user->getPassword()) {

            $session = $this->getRequest()->getSession();

            $firewall = 'secured_area';
            $token = new UsernamePasswordToken($username, $password, $firewall, array('ROLE_USER'));
            $session->set('_security_' . $firewall, serialize($token));
            $session->save();
            return new JsonResponse(array('username' => $username, 'status' => "success"));
        } else {
            return new JsonResponse(array('username' => $username, 'pass' => $password, 'status' => "bad credentials"));
        }
    }

    public function userLogoutAction()
    {
        if (isset($_COOKIE['PHPSESSID'])) {
            unset($_COOKIE['PHPSESSID']);
            setcookie('PHPSESSID', null, -1, '/');
        }
        return new JsonResponse(array('status' => "success"));
    }

    public function toggleConference($id)
    {

        $conf = $this->getDoctrine()
            ->getRepository('ConfeatureConferenceBundle:Conference')
            ->find($id);
        if ($conf->getState() == 0) {
            $conf->setState(1);
        } elseif ($conf->getState() == 1) {
            $conf->setState(0);
        }
        $em = $this->getDoctrine()->getManager();
        $em->persist($conf);
        $em->flush();
    }

    public function toggleConferenceAction($id)
    {
        if ($this->get('security.context')->isGranted('ROLE_USER')) {
            toggleConference($id);
            return new JsonResponse(array('id' => $id, 'status' => "success"));
        } else {
            return new JsonResponse(array('id' => null, 'status' => "Permission Denied"));
        }
    }

    public function viewJoin($id)
    {
        $conf = $this->getDoctrine()
            ->getRepository('ConfeatureConferenceBundle:Conference')
            ->find($id);

        $conf->setConnectedViewers($conf->getConnectedViewers() + 1);
    }

    public function streamJoin($id)
    {
        $conf = $this->getDoctrine()
            ->getRepository('ConfeatureConferenceBundle:Conference')
            ->find($id);

        $conf->setConnectedSpeakers($conf->getConnectedSpeakers() + 1);
    }

    public function viewLeave($id)
    {
        $conf = $this->getDoctrine()
            ->getRepository('ConfeatureConferenceBundle:Conference')
            ->find($id);

        $conf->setConnectedViewers($conf->getConnectedViewers() - 1);
    }

    public function streamLeave($id)
    {
        $conf = $this->getDoctrine()
            ->getRepository('ConfeatureConferenceBundle:Conference')
            ->find($id);

        $conf->setConnectedSpeakers($conf->getConnectedSpeakers() - 1);
    }

    public function viewJoinAction($id)
    {

    }

    public function streamJoinAction($id)
    {

    }

    public function viewLeaveAction($id)
    {

    }

    public function streamLeaveAction($id)
    {

    }


}
