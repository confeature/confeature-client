<?php

namespace Confeature\Bundle\ConferenceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('ConfeatureConferenceBundle:Default:index.html.twig');
    }

    public function watchAction($id)
    {
        $conf = $this->getDoctrine()
            ->getRepository('ConfeatureConferenceBundle:Conference')
            ->find($id);
        if($conf == null)
        {
            return $this->render('ConfeatureConferenceBundle:Error:notfound.html.twig', array('id' => $id));
        }
        if($conf->getPrivacy()==1)
        {
            if( isset($_POST['pass']) )
            {



                    if($conf->getViewPass() == $_POST['pass']){
                        $api = $this->get('confeature_api_services');
                        $api->viewJoin($id);
                        return $this->render('ConfeatureConferenceBundle:Default:watch.html.twig', array('conf' => $conf));
                    }else{
                        return $this->render('ConfeatureConferenceBundle:Error:badpass.html.twig');
                    }
            }
        }else{
            $api = $this->get('confeature_api_services');
            $api->viewJoin($id);
            return $this->render('ConfeatureConferenceBundle:Default:watch.html.twig', array('conf' => $conf));
        }
        return $this->render('ConfeatureConferenceBundle:Default:pass.html.twig', array('id' => $id, 'type' => "view"));

    }
    
    public function streamAction($id)
    {
        if( isset($_POST['pass']) )
        {
            $conf = $this->getDoctrine()
            ->getRepository('ConfeatureConferenceBundle:Conference')
            ->find($id);
            if($conf == null)
            {
                return $this->render('ConfeatureConferenceBundle:Error:notfound.html.twig', array('id' => $id));
            }
            if($conf->getStreamPass() == $_POST['pass']){
                $api = $this->get('confeature_api_services');
                $api->streamJoin($id);
                return $this->render('ConfeatureConferenceBundle:Default:stream.html.twig', array('id' => $id));
            }else{
                return $this->render('ConfeatureConferenceBundle:Error:badpass.html.twig');
            }
        }
            return $this->render('ConfeatureConferenceBundle:Default:pass.html.twig', array('id' => $id, 'type' => "stream"));




    }

    public function listAction()
    {
        $confs = $this->getDoctrine()
        ->getRepository('ConfeatureConferenceBundle:Conference')
        ->findBy(array('state' => '1'));

        return $this->render('ConfeatureConferenceBundle:Default:list.html.twig', array('conferences' => $confs));


    }


    public function streamHomeAction(){
        return $this->render('ConfeatureConferenceBundle:Default:streamHome.html.twig');
    }

    public function joinAction()
    {
        $api = $this->get('confeature_api_services');
        $api->streamJoin($_POST['id']);
        return $this->redirect($this->generateUrl('confeature_conference_stream', array('id' => $_POST['id'])));
    }

}
