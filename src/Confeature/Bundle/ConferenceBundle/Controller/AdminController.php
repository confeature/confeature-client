<?php

namespace Confeature\ConferenceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AdminController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('ConfeatureConferenceBundle:Admin:index.html.twig', array('name' => $name));
    }

    public function settingsAction($id)
    {
    	return $this->render('ConfeatureConferenceBundle:Admin:settings.html.twig', array('id' => $id));

    }
}
