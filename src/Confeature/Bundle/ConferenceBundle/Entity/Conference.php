<?php

namespace Confeature\Bundle\ConferenceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Conference
 */
class Conference
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $description;

    /**
     * @var integer
     */
    private $state;
    /**
     * @var \DateTime
     */
    private $started;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Conference
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Conference
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set state
     *
     * @param integer $state
     * @return Conference
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return integer 
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set started
     *
     * @param \DateTime $started
     * @return Conference
     */
    public function setStarted($started)
    {
        $this->started = $started;

        return $this;
    }

    /**
     * Get started
     *
     * @return \DateTime 
     */
    public function getStarted()
    {
        return $this->started;
    }

    /**
     * @var boolean
     */
    private $privacy;

    /**
     * @var integer
     */
    private $connectedViewers;

    /**
     * @var integer
     */
    private $connectedSpeakers;


    /**
     * Set privacy
     *
     * @param boolean $privacy
     * @return ConfInfos
     */
    public function setPrivacy($privacy)
    {
        $this->privacy = $privacy;

        return $this;
    }

    /**
     * Get privacy
     *
     * @return boolean 
     */
    public function getPrivacy()
    {
        return $this->privacy;
    }

    /**
     * Set connectedViewers
     *
     * @param integer $connectedViewers
     * @return ConfInfos
     */
    public function setConnectedViewers($connectedViewers)
    {
        $this->connectedViewers = $connectedViewers;

        return $this;
    }

    /**
     * Get connectedViewers
     *
     * @return integer 
     */
    public function getConnectedViewers()
    {
        return $this->connectedViewers;
    }

    /**
     * Set connectedSpeakers
     *
     * @param integer $connectedSpeakers
     * @return ConfInfos
     */
    public function setConnectedSpeakers($connectedSpeakers)
    {
        $this->connectedSpeakers = $connectedSpeakers;

        return $this;
    }

    /**
     * Get connectedSpeakers
     *
     * @return integer 
     */
    public function getConnectedSpeakers()
    {
        return $this->connectedSpeakers;
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="maxSpeakers", type="integer")
     */
    private $maxSpeakers;

    /**
     * @var integer
     *
     * @ORM\Column(name="maxViewers", type="integer")
     */
    private $maxViewers;

    /**
     * @var string
     *
     * @ORM\Column(name="streamPass", type="string", length=255)
     */
    private $streamPass;

    /**
     * @var integer
     *
     * @ORM\Column(name="maxResolution", type="integer")
     */
    private $maxResolution;

    /**
     * @var integer
     *
     * @ORM\Column(name="priorityMethod", type="integer")
     */
    private $priorityMethod;

    /**
     * Set maxSpeakers
     *
     * @param integer $maxSpeakers
     * @return ConfSettings
     */
    public function setMaxSpeakers($maxSpeakers)
    {
        $this->maxSpeakers = $maxSpeakers;

        return $this;
    }

    /**
     * Get maxSpeakers
     *
     * @return integer 
     */
    public function getMaxSpeakers()
    {
        return $this->maxSpeakers;
    }

    /**
     * Set maxViewers
     *
     * @param integer $maxViewers
     * @return ConfSettings
     */
    public function setMaxViewers($maxViewers)
    {
        $this->maxViewers = $maxViewers;

        return $this;
    }

    /**
     * Get maxViewers
     *
     * @return integer 
     */
    public function getMaxViewers()
    {
        return $this->maxViewers;
    }

    /**
     * Set streamPass
     *
     * @param string $streamPass
     * @return ConfSettings
     */
    public function setStreamPass($streamPass)
    {
        $this->streamPass = $streamPass;

        return $this;
    }

    /**
     * Get streamPass
     *
     * @return string 
     */
    public function getStreamPass()
    {
        return $this->streamPass;
    }

    /**
     * Set maxResolution
     *
     * @param integer $maxResolution
     * @return ConfSettings
     */
    public function setMaxResolution($maxResolution)
    {
        $this->maxResolution = $maxResolution;

        return $this;
    }

    /**
     * Get maxResolution
     *
     * @return integer 
     */
    public function getMaxResolution()
    {
        return $this->maxResolution;
    }

    /**
     * Set priorityMethod
     *
     * @param integer $priorityMethod
     * @return ConfSettings
     */
    public function setPriorityMethod($priorityMethod)
    {
        $this->priorityMethod = $priorityMethod;

        return $this;
    }

    /**
     * Get priorityMethod
     *
     * @return integer 
     */
    public function getPriorityMethod()
    {
        return $this->priorityMethod;
    }
    
    /**
     * @var boolean
     *
     * 
     */
    private $noSpeakersToggle;
    
    /**
     * @var boolean
     *
     * 
     */
    private $inactivityToggle;
    
    /**
     * @var boolean
     *
     * 
     */
    private $timerToggle;
    /**
     * @var boolean
     *
     *
     */
    private $endingHourToggle;
    
    /**
     * @var \DateTime
     * 
     */
    private $endingHour = null;
    /**
     * @var integer
     *
     */
    private $inactivityDuration = null;
    /**
     * @var integer
     *
     */
    private $timerDuration = null;


    /**
     * Set inactivityToggle
     *
     * @param integer $inactivityToggle
     * @return Conference
     */
    public function setInactivityToggle($inactivityToggle)
    {
        $this->inactivityToggle = $inactivityToggle;

        return $this;
    }

    /**
     * Get inactivityToggle
     *
     * @return integer 
     */
    public function getInactivityToggle()
    {
        return $this->inactivityToggle;
    }

    /**
     * Set timerToggle
     *
     * @param integer $timerToggle
     * @return Conference
     */
    public function setTimerToggle($timerToggle)
    {
        $this->timerToggle = $timerToggle;

        return $this;
    }

    /**
     * Get timerToggle
     *
     * @return integer 
     */
    public function getTimerToggle()
    {
        return $this->timerToggle;
    }

    /**
     * Set endingHour
     *
     * @param \DateTime $endingHour
     * @return Conference
     */
    public function setEndingHour($endingHour)
    {
        $this->endingHour = $endingHour;

        return $this;
    }

    /**
     * Get endingHour
     *
     * @return \DateTime 
     */
    public function getEndingHour()
    {
        return $this->endingHour;
    }

    /**
     * @var string
     *
     *
     */
    private $viewPass;


    /**
     * Set viewPass
     *
     * @param string $viewPass
     * @return Conference
     */
    public function setViewPass($viewPass)
    {
        $this->viewPass = $viewPass;

        return $this;
    }

    /**
     * Get viewPass
     *
     * @return string 
     */
    public function getViewPass()
    {
        return $this->viewPass;
    }

    /**
     * Set noSpeakersToggle
     *
     * @param boolean $noSpeakersToggle
     * @return Conference
     */
    public function setNoSpeakersToggle($noSpeakersToggle)
    {
        $this->noSpeakersToggle = $noSpeakersToggle;

        return $this;
    }

    /**
     * Get noSpeakersToggle
     *
     * @return boolean 
     */
    public function getNoSpeakersToggle()
    {
        return $this->noSpeakersToggle;
    }

    /**
     * Set inactivityDuration
     *
     * @param integer $inactivityDuration
     * @return Conference
     */
    public function setInactivityDuration($inactivityDuration)
    {
        $this->inactivityDuration = $inactivityDuration;

        return $this;
    }

    /**
     * Get inactivityDuration
     *
     * @return integer 
     */
    public function getInactivityDuration()
    {
        return $this->inactivityDuration;
    }

    /**
     * Set timerDuration
     *
     * @param integer $timerDuration
     * @return Conference
     */
    public function setTimerDuration($timerDuration)
    {
        $this->timerDuration = $timerDuration;

        return $this;
    }

    /**
     * Get timerDuration
     *
     * @return integer 
     */
    public function getTimerDuration()
    {
        return $this->timerDuration;
    }

    /**
     * Set endingHourToggle
     *
     * @param boolean $endingHourToggle
     * @return Conference
     */
    public function setEndingHourToggle($endingHourToggle)
    {
        $this->endingHourToggle = $endingHourToggle;

        return $this;
    }

    /**
     * Get endingHourToggle
     *
     * @return boolean 
     */
    public function getEndingHourToggle()
    {
        return $this->endingHourToggle;
    }

    public function toArray(){
        $array = toPublicArray();

        $array['maxViewers']=$this->maxViewers;
        $array['maxSpeakers']=$this->maxSpeakers;


        $array['maxResolution']=$this->maxResolution;

        $array['noSpeakersToggle']=$this->noSpeakersToggle;
        $array['inactivityToggle']=$this->inactivityToggle;
        $array['timerToggle']=$this->timerToggle;
        $array['endingHourToggle']=$this->endingHourToggle;

        $array['inactivityDuration']=$this->inactivityDuration;
        $array['timerDuration']=$this->timerDuration;
        $array['endingHour']=$this->endingHour;

        $array['streamPass']=$this->streamPass;
        $array['viewPass']=$this->viewPass;


        return $array;
    }

    public function toPublicArray(){
        $array = array();
        $array['title']=$this->title;
        $array['description']=$this->description;

        $array['privacy']=$this->privacy;

        return $array;
    }
}
