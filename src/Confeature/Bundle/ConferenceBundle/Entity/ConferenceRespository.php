<?php

namespace Confeature\ConferenceBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * ConferenceRepository
 *
 *
 */
class ConferenceRepository extends EntityRepository
{

    public function getOnlineConfs()
    {
        return $this->createQueryBuilder('c')
            ->where('c.state = 1')
            ->setParameter('enabled', true)
            ->addOrderBy('c.started', "DESC")
    }
}